package mods_toml_maker.com.legacy.mods_toml_updater;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ModsTomlUpdater
{
	static final String TAG_PRE = "${", TAG_POST = "}";

	//
	// Replacements are formatted as:
	// 		${gradle_properties_field}
	// 		${gradle_properties_field : default_value}
	//
	// Writing can be enabled/disabled with (writing must be manually re-enabled after being disabled):
	//		${requirement = gradle_properties_field}
	// 		${requirement = gradle_properties_field_a & gradle_properties_field_b & gradle_properties_field_c}
	// 		${requirement = null}
	//
	//
	public static void main(String[] args)
	{
		try
		{
			// Default values
			String gradlePropertiesFile = "gradle.properties";
			String templateFile = "neoforge.mods.toml";
			List<String> writePaths = List.of("src/main/resources/META-INF/neoforge.mods.toml");

			// Read arguments
			for (String arg : args)
			{
				String key = "properties=";
				if (arg.startsWith(key))
				{
					gradlePropertiesFile = arg.substring(key.length());
				}

				key = "template=";
				if (arg.startsWith(key))
				{
					templateFile = arg.substring(key.length());
				}

				key = "write_paths=";
				if (arg.startsWith(key))
				{
					String val = arg.substring(key.length());
					if (val.startsWith("[") && val.endsWith("]"))
					{
						writePaths = Arrays.stream(val.substring(1, val.length() - 1).split(",")).map(String::trim).collect(Collectors.toList());
					}
					else
					{
						writePaths = List.of(val.trim());
					}
				}
			}

			// Read values from gradle.properties
			Map<String, String> properties = new HashMap<>();
			File propertiesFile = new File(gradlePropertiesFile);
			if (propertiesFile.exists())
			{
				Scanner reader = new Scanner(propertiesFile);
				while (reader.hasNextLine())
				{
					String line = reader.nextLine();
					if (line.startsWith("#"))
						continue;

					int equalsSign = line.indexOf('=');
					if (equalsSign == -1)
						continue;

					properties.put(line.substring(0, equalsSign), line.substring(equalsSign + 1));
				}
				reader.close();
			}

			//System.out.println(substitute("version=\"${mod_version : 1.0.0}\"", properties));

			File tomlTemplate = new File(templateFile);
			if (tomlTemplate.exists())
			{
				// Get and process mods.toml template
				List<String> lines = new ArrayList<>();
				Scanner reader = new Scanner(tomlTemplate);
				boolean shouldAdd = true;
				while (reader.hasNext())
				{
					String line = reader.nextLine();
					String trimmed = line.trim();
					// Ignore comment lines for generation
					if (trimmed.startsWith("#"))
						continue;

					// Requirement tag lines aren't written, but they set the flag for if we should write or not
					String requiresPre = "${requirement";
					String requiresPost = "}";
					String equals = "=";
					String not = "!";
					if (trimmed.startsWith(requiresPre) && trimmed.endsWith(requiresPost) && trimmed.contains(equals))
					{
						String requirement = trimmed.substring(trimmed.indexOf(equals) + 1, trimmed.length() - requiresPost.length());
						boolean passes = true;
						for (String field : requirement.split("&"))
						{
							String trimmedField = field.trim();
							boolean shouldContain = !trimmedField.startsWith(not);
							String f = shouldContain ? trimmedField : trimmedField.replaceFirst(not, "");
							boolean contains = properties.containsKey(f);
							String prop = properties.get(f);
							boolean passCondition;
							if (prop != null && (prop.toLowerCase(Locale.US).equals(Boolean.TRUE.toString()) || prop.toLowerCase(Locale.US).equals(Boolean.FALSE.toString())))
							{
								passCondition = Boolean.parseBoolean(prop.toLowerCase(Locale.US));
							}
							else
							{
								passCondition = shouldContain == contains;
							}

							// If we should not have it matches with if we do have it, fail the condition (or inverse)
							if (!passCondition)
							{
								passes = false;
								break;
							}
						}
						shouldAdd = passes || requirement.trim().equals("null") || requirement.isBlank();
					}
					else
					{
						if (shouldAdd)
							lines.add(substitute(line, properties));
					}
				}
				reader.close();

				// Save the new mods.toml to the write paths
				for (String writePath : writePaths)
				{
					File file = new File(writePath);
					file.getParentFile().mkdirs();
					PrintWriter test = new PrintWriter(file);
					for (String line : lines)
					{
						test.println(line);
					}
					test.close();
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	static String substitute(String line, Map<String, String> properties)
	{
		String splitter = ":";

		// Replaces all instances of ${property} with the gradle.properties value, allowing for defaults
		while (line.contains(TAG_PRE) && line.contains(TAG_POST))
		{
			int start = line.indexOf(TAG_PRE);
			int end = line.indexOf(TAG_POST);

			if (start <= end)
			{
				String contents = line.substring(start + TAG_PRE.length(), end);

				String replacement;
				String defaultVal;
				if (contents.contains(splitter))
				{
					int splitStart = contents.indexOf(splitter);
					int splitEnd = splitStart + splitter.length();

					replacement = properties.get(contents.substring(0, splitStart).trim());
					defaultVal = contents.substring(splitEnd).trim();
				}
				else
				{
					replacement = properties.get(contents.trim());
					defaultVal = null;
				}

				if (replacement == null && defaultVal != null)
					replacement = defaultVal;
				if (replacement == null)
					throw new NullPointerException("No property exists for " + TAG_PRE + contents + TAG_POST + " and no default value was set.");
				line = line.substring(0, start) + replacement + line.substring(end + TAG_POST.length());
			}
		}
		return line;
	}
}
