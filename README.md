# Mods Toml Generator

Used to generate a mods.toml file for NeoGradle workspaces. This is more of a personal project, so it's more tailored for my usecases with NeoGradle. It may work for ForgeGradle or whatever else you need with proper modification, but idk.

Use this project however you want. Built executable jar files are in the `builds` folder.

## For NeoGradle `build.gradle` files

```groovy
plugins {
	id 'java'
}
```

This replaces the NeoGradle `tasks.withType(ProcessResources)` block. It generates the mods.toml in your `src` folder on gradle refresh, game load, and build.
```groovy
// Generates mods.toml file. The root folder for each file in args is the workspace folder. You can have more than one write path if needed, just separate with commas [path, path, path].
tasks.register('genModsTomlNeo', JavaExec) {
	classpath = files('workspace_gen/mods_toml_generator.jar')
	args 'properties=gradle.properties', 'template=mods.toml', 'write_paths=[src/main/resources/META-INF/mods.toml]'
}

// cacheLauncherMetadata is the earliest gradle task that gets called on build, refresh, and run. If this task doesn't exist, pick a different one one.
tasks.named('cacheLauncherMetadata').get().dependsOn 'genModsTomlNeo'
```